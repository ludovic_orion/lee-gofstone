import { Form, Field } from 'react-final-form';
import { useState } from 'react';
import { Alert } from 'react-bootstrap';
import { Link, Redirect } from "react-router-dom";
import { createAccount } from './APIRequests';
import './css/form.css';


function FormulaireCompte() {
  const [err, setErr] = useState(null);
  const [succes, setSucces] = useState(null);
  const onSubmit = (compte) => {
    if (!compte.pseudo) {
      console.log(compte.name)
      setErr("Veuillez entrer un nom d'utilisateur")
      return;
    }
    if (!compte.mdp) {
      setErr("Veuillez entrer un mot de passe")
      return;
    }

    if (compte.mdpConf !== compte.mdp) {
      setErr("Les mots de passe ne correspondent pas")
      return;
    }
    //Creation de compte grâce à un pseudo, un mail, un mdp envoyé au serveur
    const send = async () => {
      try {
        let result = await createAccount(compte.pseudo, compte.mail, compte.mdp)
        setSucces(result)


      } catch (e) {
        if (e.response.data === "User already exists") {
          setErr("Ce compte existe déjà")
        }
      }
    }
    send();
  }
  //Redirection vers la page de connexion quand le compte est créé
  if (succes !== null) {
    return <Redirect to="/Connexion" />
  } else {
    return (
      <div className="container ctn">
        <h1 className="text-white display-4">Inscription</h1>
        <Form onSubmit={onSubmit}
          render={({ handleSubmit }) => (
            <form className="formulaireCompte" onSubmit={handleSubmit}>

              <div className="form-group">
                <Field id="pseudo" name="pseudo" component="input" type="text" placeholder="Saisir un pseudo" className="form-control" />
              </div>

              <div className="form-group">
                <Field id="mail" name="mail" component="input" type="email" placeholder="Saisissez votre adresse mail" className="form-control" />
              </div>

              <div className="form-group">
                <Field id="mdp" name="mdp" component="input" type="password" placeholder="Saisir un mot de passe" className="form-control" />
              </div>

              <div className="form-group">
                <Field id="mdpConf" name="mdpConf" component="input" type="password" placeholder="Confirmez votre mot de passe" className="form-control" />
              </div>
              {err ? <Alert variant="danger">{err}</Alert> : ""}
              <button className="btn btn-primary" type="submit">
                S'inscrire
            </button>

              <div className="scdLink">
                <Link className="txt " to="/Connexion">Déjà inscrit ? Connecte-toi !</Link>
              </div>
            </form>

          )} />
      </div>

    );
  }
}

export default FormulaireCompte;
