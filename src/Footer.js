import React from "react";

class Footer extends React.Component {
  
    
    render() {
        return (
        <footer className="px-3">
            <p>Copyright &copy; Web2 L3 UT2J</p>
        </footer>
        );
            
        }
}
export default Footer;