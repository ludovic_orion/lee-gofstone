import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { infoMatch, getMatchmakingParticipate, getMatchmakingUnParticipate, getAllMatchmaking, askPlayerRequest, acceptRequest } from './APIRequests';
import './css/form.css'
import './css/matchmaking.css'
 


function Matchmaking() {
    const token = useSelector((state) => state.isTokenOk)
    const tokenVal = useSelector((state) => state.tokenValue)
    const [myRequest, setMyRequest] = useState([]);
    const [recherche, setRecherche] = useState(false);
    const [allUserMM, setAllUserMM] = useState(null);
    const [acceptedRequest, setAcceptedRequest] = useState(null);
    const [adversaireId, setAdversaireId] = useState(0);

    

    useEffect(() => {



        const timer = setInterval(async () => {
            if (recherche) {
                const allusers = await getAllMatchmaking(tokenVal);
                setAllUserMM(allusers);

                const dataParticipate = await getMatchmakingParticipate(tokenVal)
                setMyRequest(dataParticipate.data.request);

                const checkMatch =await infoMatch(tokenVal);
                setAcceptedRequest(checkMatch)
            }

        }, 3000);
        return () => clearInterval(timer);





    })

    document.title="League of Stones - Matchmaking"

    //Ajoute l'utilisateur à la liste du matchmaking
    const addMeToTheWaitingList = async () => {
        try {
            let dataParticipate = await getMatchmakingParticipate(tokenVal)

            setMyRequest(dataParticipate.data.request);
            setRecherche(true);
        } catch (error) {
            console.log(error)
        }

    }
    //Enlève l'utilisateur de la liste du matchmaking
    const removeMeToTheWaitingList = async () => {
        await getMatchmakingUnParticipate(tokenVal)
        setMyRequest([]);
        setRecherche(false);
        setAllUserMM(null);
    }

    //L'utilisateur accepte une demande d'un joueur parmis la liste des joueurs ayant demandé à jouer avec lui
    const AccepterRequest = async (event) => {
        const accepterRequesto = await acceptRequest(tokenVal, event.target.id)
        setAcceptedRequest(accepterRequesto)

    }
    //L'utilisateur propose à un joueur de jouer contre lui parmis les joueurs dans la liste de ceux qui recherchent
    const DemanderRequest = async (event) => {
        setAdversaireId(event.target.id)
        await askPlayerRequest(tokenVal, event.target.id)


    }
    //Affichage de tous les joueurs demandant à jouer contre vous
    let joueurMM = null;
    if (myRequest !== null) {
        joueurMM = myRequest.map((user) => {
            return (
                <div className="ctnJoueur">
                    <h2 className="text-white">{user.name} </h2>
                    <button className="btn btn-warning" id={user.matchmakingId} onClick={AccepterRequest}>Accepter</button>
                    {adversaireId === user.matchmakingId ? <p className="text-white">En attente de connexion...</p> : <p></p>}
                </div>


            )
        })

    }
    else {
        joueurMM = <h1>Pas d'adversaires</h1>
    }


    //Affichage de tous les joueurs cherchant une partie
    let joueurAllMM = null;
    if (allUserMM) {
        joueurAllMM = allUserMM.data.map((users) => {
            return (
                <div className="ctnJoueur">
                    <h3 className="text-white">Nom : {users.name}</h3>

                    <button className="btn btn-warning" id={users.matchmakingId} onClick={DemanderRequest}>Jouer contre ce joueur</button>
                    {adversaireId === users.matchmakingId ? <p className="text-white">En attente d'une reponse...</p> : <p></p>}



                </div>

            )
        })
    }
    //Si la personne n'est pas connectée redirige vers connexion
    if (!token) {
        return <Redirect to='/Connexion' />
    }
    //Si la requete est accéptée redirige vers le match
    if (acceptedRequest) {
        return <Redirect to='/Match' />
    }

    else {
        return (
            <section>
                <div className="container ctn gameFind">
                    {joueurAllMM ? <h1 className="text-white">Partie(s) Trouvée(s) !</h1> : <h1 className="text-white" >Aucune partie disponible</h1>}
                </div>
                <button className="btn-valid" onClick={addMeToTheWaitingList}>Lancer le matchmaking</button>
                <br />
                <button className="btn-cancel" onClick={removeMeToTheWaitingList}>Annuler le matchmaking</button>

                <div className=" listPlayer row">
                    <div className="listPlayer1 col">
                        <h2 className="text-white ">Liste des joueurs voulant jouer contre vous</h2>
                        {joueurMM}
                    </div>
                    <div className="listPlayer1 col">
                        <h2 className="text-white ">Liste des joueurs en recherche</h2>
                        {joueurAllMM}
                    </div>
                </div>
            </section>


        )
    }

}

export default Matchmaking;
