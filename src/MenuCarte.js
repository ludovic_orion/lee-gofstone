import React, { useState, useEffect } from "react";
import "./css/MenuCarte.css";
import Card from "./Card.js";
import { afficherCartes, infoMatch, initDeck } from "./APIRequests";
import { useSelector } from "react-redux"
import { Redirect } from 'react-router-dom';

function MenuCarte({ nbCards }) {
  const token = useSelector((state) => state.isTokenOk)
  const tokenVal = useSelector((state) => state.tokenValue);
  const [deck, setDeck] = useState([]);
  const [champions, setChampions] = useState([]);
  const [deckValidated, setDeckValidated] = useState(false);
  const [deckV, setDeckV] = useState(null);
  const [rdy, setRdy] = useState(false);

  document.title="League of Stones - Choix des cartes"

  useEffect(() => {
    if (deckV) {
      const initdeck = async () => {
        await initDeck(tokenVal, JSON.stringify(deckV))
      }
      initdeck();
    }
    
    if (champions.length === 0) {
      const allCartes = async () => {

        let allcards = await afficherCartes()
        setChampions(allcards.data)
      }
      allCartes();
    }

    const timer = setInterval(async () => {
      const match = await infoMatch(tokenVal)
      if (match.data.status !== "Deck is pending") {
        setRdy(true)

      }
    }, 3000);
    return () => clearInterval(timer);
  }



  )

  function handleClick(championId) {
    const deckCopy = [...deck];
    const champsCopy = [...champions];
    let championIndex = champsCopy.findIndex((item) => { return item.id === championId });
    if (championIndex !== -1) { // click is on a champion card
      deckCopy.push(champsCopy[championIndex]); // add card in deck
      champsCopy.splice(championIndex, 1); //remove card from champions
    }
    else {//click is on deck card
      championIndex = deckCopy.findIndex((item) => { return item.id === championId });
      champsCopy.push(deckCopy[championIndex]); // readd card in champions
      deckCopy.splice(championIndex, 1); //remove card from deck
    }

    setChampions(champsCopy);
    setDeck(deckCopy);

  }
  function handleClickValidated() {
    setDeckValidated(true);
    let keyDeck = [];
    deck.forEach(card => keyDeck.push({ "key": card.key }));
    setDeckV(keyDeck);
  }
  
  //Si la personne n'est pas connectée redirige vers connexion
  if (!token) {
    return <Redirect to='/Connexion' />
  }


  if (rdy) {
    return <Redirect to="/Match" />
  }
  let deckCompleted = deck.length === nbCards;

  const validateButton = <button className="btn btn-primary bouttonValidate" onClick={handleClickValidated}>J'ai fini !</button>

  const displayValidateButton = deckCompleted ? validateButton : null;

  let deckInProgress =
    <main className="container-fluid flex-fill d-flex  justify-content-center p-0">
      <section className="container-fluid flex-fill py-2 d-flex flex-column justify-content-center champCarte">
        <h2 className="display-4">Champions disponibles</h2>
        <section className="row justify-content-center pb-2">
          {champions.map(champion => {
            return (<Card key={champion.id} champion={champion} clickHandler={!deckCompleted ? handleClick : () => { }} />)
          })}
        </section>
      </section>
      <section className="container-fluid flex-fill py-2 d-flex flex-column justify-content-center champCarte">
        <h2 className="display-4">Mon deck {displayValidateButton}</h2>
        <section className="row justify-content-center pb-2">
          {deck.map(champion => {
            return (<Card key={champion.id} champion={champion} clickHandler={!deckValidated ? handleClick : () => { }} />)
          })}
        </section>
      </section>
    </main>;


  let deckSuccess =
    <main className="container-fluid flex-fill d-flex  justify-content-center p-0">
      <div className="container-fluid flex-fill py-2 d-flex flex-column border-left border-1 champCarte">
        <h1>Le deck a été validé !</h1>
        <section className="row justify-content-center pb-2">
          {deck.map(champion => {
            return (<Card key={champion.id} champion={champion} clickHandler={!deckValidated ? handleClick : () => { }} />)
          })}
        </section>
      </div>
    </main>

  return (
    <section>

      {deckValidated ? deckSuccess : deckInProgress}
    </section>
  );
}




export default MenuCarte;
