import { useEffect, useState } from "react/cjs/react.development";
import { infoMatch, attaquerCarte, attaquerJoueur, endTurn, finirMatch, playCard } from "./APIRequests";
import { useSelector } from 'react-redux';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

import { Redirect } from "react-router-dom";
import Img1 from "./img/barbare.png";
import Img2 from "./img/warrior.png";
import './css/match.css';

// Pour outcome
// En dehors de Match afin de ne pas les réévaluer à chaque rendu
const WON = "won", LOST = "lost", ONGOING = "ongoing";
const NOT_CONNECTED = "You need to be connected", DECK_PENDING = "Deck is pending";

function Match() {
    const tokenVal = useSelector((state) => state.tokenValue);
    const id_joueur = useSelector((state) => state.id)
    const [infoP, setInfoP] = useState(false);
    const [attacker, setAttacker] = useState(null);
    const [matchStatus, updateMatchStatus] = useState(undefined);
    const [msgRetour, setMsgRetour] = useState(null);
    const [champBloquer, setchampBloquer] = useState([]);
    let cartesMain, cartesPlateau, cartesPlateauEnnemi, deck, mainJoueur, mainEnnemie, tour, carteMainEnnemi;
    cartesMain = cartesPlateau = cartesPlateauEnnemi = deck = mainEnnemie = mainJoueur = tour = carteMainEnnemi = null;

    const defaultTooltipDelays = { show: 0, hide: 50 };

    document.title = "League of Stones - Match"

    useEffect(() => {

        //Intervalle reverifiant les infos du match
        const info = setInterval(async () => {
            setMsgRetour(null)

            await infoMatch(tokenVal).then(partie => {
                updateMatchStatus(partie.data.status === DECK_PENDING ? DECK_PENDING : ONGOING);
                setInfoP(partie);
            }).catch(e => {
                const errmsg = e.message;
                switch (errmsg) {
                case NOT_CONNECTED:
                    updateMatchStatus(errmsg);
                    break;
                case "There is no match associated":
                    updateMatchStatus(null);
                    break;
                default:
                    // il y a une erreur dans le serveur
                    throw e;
                }
            });

            if (infoP && infoP.data.status.endsWith(WON))
                // Le serveur a envoyé une requête indiquant qu'un joueur a gagné.
                // Sachant que ce code n'est pas exécuté à la demande du joueur
                // et que le joueur a perdu.
                finirMatch(tokenVal).then(p => updateMatchStatus(LOST));
        }, 3000)
        return () => clearInterval(info);


    });

    //Le joueur pioche une carte
    const pickIndeck = async () => {
        if (tour) {
            await pickCard(tokenVal)
                .then((data) => {
                    champBloquer.push(data.data.key)

                })
                .catch((error) => {
                    console.log(error)
                    setMsgRetour("Vous avez déjà pioché une carte !")
                })
        } else {
            setMsgRetour("Attendez votre tour pour jouer")
        }


    }



    //Attaque l'autre joueur directement si c'est possible
    const AttackPlayer = async () => {
        if (tour) {
            if (attacker == null) {
                setMsgRetour("Selectionnez un champion pour attaquer !")
            } else {
                if (champBloquer.includes(attacker)) {
                    console.log("cc")
                    setMsgRetour("Vous pourrez attaquer au prochain tour")
                }
                else {
                    if (mainEnnemie.board.length !== 0) {
                        setMsgRetour("Le plateau adverse doit être vide pour attaquer le joueur !")
                    } else {
                        if (attacker.attack !== 0) {
                            await attaquerJoueur(tokenVal, attacker).catch(e => {
                                setMsgRetour("Vous ne pouvez pas attaquer 2 fois avec une même carte !")
                                setAttacker(null);
                            });

                            infoMatch(tokenVal).then(partie => {
                                if (partie.data.status.endsWith(WON))
                                    updateMatchStatus(WON);
                            }).catch(e => setAttacker(null));
                        }

                    }
                }
            }

        } else {
            setMsgRetour("Attendez votre tour pour jouer")
        }




    }
    //Permet de finir son tour
    const endTurnPlayer = async () => {
        await endTurn(tokenVal)
        setMsgRetour(null)
        setchampBloquer([])
    }
    //Permet de poser une carte sur le board
    const poserCarte = async (event) => {
        if (tour) {
            if (champBloquer.includes(event.target.id)) {
                setMsgRetour("Vous devez attendre le prochain tour pour poser cette carte !")
            }
            else {
                await playCard(tokenVal, event.target.id)
                    .catch(() => {
                        setMsgRetour("Plus de place disponible sur votre plateau !")
                    })
                champBloquer.push(event.target.id)
            }
        }
    }
    //Permet de choisir une carte pour attaquer
    const choisirCarte = async (event) => {
        if (tour) {
            setAttacker(event.target.id)
        }
    }
    //Permet d'attaquer le board ennemi après avoir choisi une carte du board pour l'attaquer
    const attaquerEnnemi = async (event) => {

        if (tour) {
            if (!attacker) {
                setMsgRetour("Vous devez choisir une carte de votre plateau pour attaquer !")
            } else {
                if (attacker.attack === 0) {
                    setMsgRetour("Vous avez déjà attaqué avec cette carte !")
                } else {
                    if (champBloquer.includes(attacker)) {
                        setMsgRetour("Vous pourrez attaquer au prochain tour")

                    } else {
                        if (attacker.attack !== 0) {
                            await attaquerCarte(tokenVal, attacker, event.target.id).catch(e => {
                                setMsgRetour("Vous pourrez attaquer au prochain tour")
                                setAttacker(null)
                            });

                            infoMatch(tokenVal).then(partie => {
                                if (partie.data.status.endsWith(WON))
                                    updateMatchStatus(WON);
                            }).catch(e => setAttacker(null));
                        }
                    }
                }
            }

        } else {
            setMsgRetour("Attendez votre tour pour jouer")
        }
    }

    // Redirection
    switch (matchStatus) {
    case DECK_PENDING:
        return <Redirect to="/MenuCarte" />;
    case NOT_CONNECTED:
        return <Redirect to="/FormulaireCompte" />;
    case WON:
    case LOST:
    case null:
        return <Redirect to="/Home" />
    default:  // Inclus pour satisfaire le linter
    }
 
    if (infoP && infoP.data.player1 && infoP.data.player2) {

        if (infoP.data.player1.id === id_joueur) {

            mainJoueur = infoP.data.player1
            mainEnnemie = infoP.data.player2
        }
        if (infoP.data.player2.id === id_joueur) {

            mainJoueur = infoP.data.player2
            mainEnnemie = infoP.data.player1
        }

        if (mainJoueur) {
            //tour = infoP.data.status === "Turn : player 1" ? mainJoueur.name === infoP.data.player1.name : infoP.data.status === "Turn : player 2" ? mainJoueur.name === infoP.data.player2.name : false;
            tour = mainJoueur.turn

            deck = <button className="nbDeck joueurNbDeck" onClick={pickIndeck} >{mainJoueur.deck}</button>

            let tab = []

            for (var i = 0; i < mainEnnemie.hand; i++) {
                tab.push(1)
            }

            carteMainEnnemi = tab.map(() => {
                return (
                    <div className="joueur2_carteHand">
                        <p>1</p>
                    </div>
                )
            });


            //Verifie qu'il y ai des cartes dans la main du joueur, s'il n'y en a pas affiche un texte sinon affiche les cartes        
            if (mainJoueur.hand) {
                if (mainJoueur.hand.length === 0) {
                    cartesMain = ""
                }
                else {

                    cartesMain = mainJoueur.hand.map((carteMain) => {
                        const actionMsg = props => <Tooltip {...props}>Mettre sur le plateau</Tooltip>;
                        const championImgUrl =
                            "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/" +
                            carteMain.key +
                            "_0.jpg";
                        return (
                            <OverlayTrigger placement="top" delay={defaultTooltipDelays} overlay={actionMsg}>
                                <div className="main card carte" onClick={poserCarte}>
                                    <img id={carteMain.key} className="card-img-top" src={championImgUrl} alt={carteMain.key} />
                                    <div className="mainCard card-body ">
                                        <p id={carteMain.key} className="mainText taille text-muted card-title">{carteMain.name}</p>
                                        <p id={carteMain.key} className="mainText taille text-muted card-title">Att : {carteMain.info.attack}</p>
                                        <p id={carteMain.key} className="mainText taille text-muted card-title">Def : {carteMain.info.defense}</p>
                                    </div>
                                </div>
                            </OverlayTrigger>
                        )
                    })
                }
            }

            //Verifie qu'il y ai des cartes sur le tableau du joueur, s'il n'y en a pas affiche un texte sinon affiche les cartes 
            if (mainJoueur.board.length === 0) {
                cartesPlateau = <p className="joueur_platau_vide">Votre plateau est vide. Le joueur adverse peut vous attaquer !</p>
            }
            else {
                cartesPlateau = mainJoueur.board.map((cartePlateau) => {
                    const actionMsg = props => <Tooltip {...props}>Attaquer</Tooltip>;

                    const championImgUrl =
                        "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/" +
                        cartePlateau.key +
                        "_0.jpg";
                    return (
                        <OverlayTrigger placement="top" delay={defaultTooltipDelays} overlay={actionMsg}>
                            <div className="main card carte" onClick={choisirCarte}>
                                <img id={cartePlateau.key} className="card-img-top" src={championImgUrl} alt={cartePlateau.key} />
                                <div className="mainCard card-body ">
                                    <p id={cartePlateau.key} className="mainText taille text-muted card-title">{cartePlateau.name}</p>
                                    <p id={cartePlateau.key} className="mainText taille text-muted card-title">Att : {cartePlateau.info.attack}</p>
                                    <p id={cartePlateau.key} className="mainText taille text-muted card-title">Def : {cartePlateau.info.defense}</p>
                                </div>
                            </div>

                        </OverlayTrigger>
                    )
                })
            }

            //Verifie qu'il y ai des cartes sur le tableau adverse, s'il n'y en a pas affiche un texte sinon affiche les cartes 
            if (mainEnnemie.board.length === 0) {
                cartesPlateauEnnemi = <p className="joueur_platau_vide">L'ennemi n'a plus de cartes. Attaquez-le !</p>
            }
            else {
                cartesPlateauEnnemi = mainEnnemie.board.map((carteEnnemie) => {
                    const actionMsg = props => <Tooltip {...props}>Attaquer</Tooltip>;

                    const championImgUrl =
                        "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/" +
                        carteEnnemie.key +
                        "_0.jpg";
                    return (
                        <OverlayTrigger placement="bottom" delay={defaultTooltipDelays} overlay={actionMsg}>
                            <figure className="main card carte" onClick={attaquerEnnemi}>
                                <img id={carteEnnemie.key} className="card-img-top" src={championImgUrl} alt={carteEnnemie.key} />
                                <figcaption className="mainCard card-body ">
                                    <p id={carteEnnemie.key} className="mainText taille text-muted card-title">{carteEnnemie.name}</p>
                                    <p id={carteEnnemie.key} className="mainText taille text-muted card-title">Att : {carteEnnemie.info.attack}</p>
                                    <p id={carteEnnemie.key} className="mainText taille text-muted card-title">Def : {carteEnnemie.info.defense}</p>
                                </figcaption>
                            </figure>

                        </OverlayTrigger>

                    )

                })
            }
        }
    }


    //Tant que les données ne sont pas récupérées affiche qu'un match est en cours... (et va donc être bientôt chargé)
    if (infoP === null) {
        return <h1>Match en cours...</h1>
    }
    else {
        if (infoP && mainJoueur && mainEnnemie) {
            const actionMsg = props => <Tooltip {...props}>Attaquer</Tooltip>;

            return (
                <div className="plateau">

                    <div className="joueur2">
                        <div className="bloc_msg">
                            <div className="msgTurn">
                                <p>{mainJoueur.turn ? "C'est votre tour !" : "L'adversaire joue..."}</p>
                            </div>
                            <div className="msgToPlayer">
                                <p className="paraMsgToPlayer">{msgRetour}</p>
                            </div>
                        </div>

                        <div className="joueur2_hand">
                            {carteMainEnnemi}
                        </div>
                        <div className="attenteJoueur">
                            <p className="text-danger display-1 msgTurn "></p>
                        </div>
                        <div className="joueur2_plateau">
                            {cartesPlateauEnnemi}
                        </div>
                        <div className="joueur2_deck">
                            <p className="nbDeck">{mainEnnemie.deck}</p>
                        </div>
                        <div className="joueur2_img">
                            <img className="joueur_img" src={Img1} alt="Joueur2" />
                        </div>
                        <OverlayTrigger placement="right" delay={defaultTooltipDelays} overlay={actionMsg}>
                            <div className="joueur2_hp">
                                <p onClick={AttackPlayer} className={mainEnnemie.hp < 50 ? "para2_hp joueur_low" : "para2_hp"} >{mainEnnemie.hp} | 150</p>
                            </div>
                        </OverlayTrigger>

                    </div>
                    <hr />
                    <div className="joueur1">
                        <div className="joueur1_plateau">
                            {cartesPlateau}
                        </div>
                        <div className="joueur1_deck">
                            {deck}
                        </div>

                        <div className="joueur1_img">
                            <img className="joueur_img" src={Img2} alt="Joueur1" />
                        </div>
                        <div className="joueur1_hand">
                            {cartesMain}
                        </div>
                        <div className="joueur1_hp">
                            <p className={mainJoueur.hp < 50 ? "para2_hp joueur_low" : "para2_hp"} >{mainJoueur.hp} | 150</p>
                        </div>
                        <div>
                            <button className="end btn btn-danger" onClick={endTurnPlayer} hidden={!mainJoueur.turn}>Finir votre tour</button>
                        </div>
                    </div>



                </div>
            )
        }
        else {
            return <h1>Match en cours...</h1>
        }

    }
}
export default Match;
