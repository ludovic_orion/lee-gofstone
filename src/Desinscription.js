import { Form, Field } from 'react-final-form';
import { Link, Redirect } from "react-router-dom";
import { desinscrire } from './APIRequests';
import { useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { userDisconnected } from "./actions";
import './css/form.css';

function Desinscription() {
    let dispatch = useDispatch();
    const email = useSelector((state) => state.email)
    const tokenVal = useSelector((state) => state.tokenValue)
    const [etat, setEtat] = useState(false);

    //Lorsque le formulaire est fourni la désinscription est lancée
    const Desinscrire = (desinscription) => {
        desinscrire(desinscription.password,email , tokenVal)
        dispatch(userDisconnected())
        setEtat(true)

    }
    //Redirection vers la page d'accueil
    if (etat) {
        return <Redirect to="/Home" />
    }
    else {
        return (

            <Form
                onSubmit={Desinscrire}
                render={({ handleSubmit }) => (
                    <div className="container ctn">

                        {tokenVal ? null : <Redirect to="/Home" />}

                        <h1 className="text-white">Formulaire de désinscription</h1>
                        <form className="form" onSubmit={handleSubmit}>
                            <div className="form-group">
                                <label className="text-white">Confirmez votre Mot de passe pour vous désinscrire</label>
                                <Field
                                    name="password"
                                    component="input"
                                    placeholder="Mot de passe"
                                    type="password"
                                    className="form-control"
                                />
                            </div>
                            <button className="btn btn-primary" type="submit">
                                Se Desinscrire
                    </button>
                            <div className="scdLink">
                                <Link className="txt" to="/Home">Annuler la désinscription</Link>
                            </div>
                        </form>
                    </div>

                )}
            />
        )
    }
}
export default Desinscription;