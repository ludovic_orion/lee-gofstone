import { useDispatch, useSelector } from 'react-redux';
import { userDisconnected } from "./actions";
import { Link, Redirect } from 'react-router-dom';
import { disconnect } from './APIRequests';
import { useState } from "react";
import './css/Deconnexion.css';

function Deconnexion() {
    
    const tokenVal = useSelector((state) => state.tokenValue);
    let dispatch = useDispatch();
    const [deconnexion, setDeconnexion] = useState(null);

    const Deconnecter = () => {
        let rep = disconnect(tokenVal)
        setDeconnexion(rep)
        dispatch(userDisconnected())

    }
    //Redirige vers Home quand la deconnexion est réussie
    if (deconnexion) {
        return <Redirect to="/Home" />
    }
    //Bouton demandant confirmation avant de deconnecter la personne 
    else {
        return (

            <section className="container ctn">
                <h1 className="text-white" >Voulez vous vraiment vous déconnecter ?</h1>
                <div>
                    <button id="disconnect-btn" className="btn btn-light" onClick={Deconnecter}>Se déconnecter</button>
                    <Link id="back-btn" className="nav-link active " to="/Home">Retour à l'accueil</Link>
                </div>
            </section>)
            ;
    }
}
export default Deconnexion;