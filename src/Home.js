import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import { infoMatch } from './APIRequests';
import './css/Home.css';


//Page d'accueil avec les règles du jeux
function Home() {
  const loggedIn = useSelector((state) => state.isTokenOk);
  const tokenVal = useSelector((state) => state.tokenValue);
  const [checkMatch, setCheckMatch] = useState(null);
  let dst = loggedIn ? "/Matchmaking" : "/FormulaireCompte";
  let txt = "Jouer";
  //Verifie s'il y a un match quand on est connecté
  useEffect(() => {
    if (checkMatch === null && loggedIn) {
      const verif = async () => {
        let info = await infoMatch(tokenVal)
        setCheckMatch(info)

      }
      verif();
    }

  })



  //bouton redirigeant l'utilisateur vers la page de matchmaking 
  //ou vers le formulaire de création de compte sinon

  const PlayBtn = () => {
    //S'il y a un match on change le bouton et la redirection
    if (checkMatch) {
      txt = "Reprendre la partie"
      dst = "/Match"
    }
    return (
      <div className="play-btn row justify-content-center">
          <Link className="btn btn-danger btn-lg translate-middle text-center" to={dst}>{txt}</Link>
      </div>
      
    )
  }
  const consulterCartes = () => {
    return (
      <div class="play-btn row justify-content-center">
        <Link className="btn btn-danger btn-lg translate-middle text-center" to="/ConsulterCartes">Consulter les cartes</Link>
      </div>
    )
  }


  document.title = "League of Stones – Accueil";

  return (
    <section id="rules" class="text-left px-3">
      <h1 className="text-center" >Bienvenue sur le site Lee GofStone !</h1>
      <article className="boutons_jeu">
      {PlayBtn()}
      {consulterCartes()}
      </article>
      <h2>Comment jouer ?</h2>
      
      <p>
        Dans <em>League of Stones</em>, vous êtes associé à un adversaire en
        ligne ayant un niveau similaire au vôtre afin de jouer une partie.
        Vous commencez la partie avec <strong>4 cartes</strong> (représentant
        chacune un <em>champion</em>) et <strong>150 points de vie</strong>.
      </p>
      <p>
        Votre objectif: <strong>descendre les points de vie de votre adversaire à 0</strong>.
      </p>
      <p>
        Pour cela vous et votre adversaire jouerez à tour de rôle, en
        ayant la possibilité à chaque tour de choisir entre trois options:
      </p>
      <h3>Piocher une carte</h3>
      <h3>Poser une carte sur le plateau</h3>
      <ul>
        <li>Vous ne pouvez pas avoir plus de <strong>5 cartes</strong> sur le plateau en même temps.</li>
        <li>Une fois une carte posée sur le plateau, vous ne pouvez pas la retirer.</li>
      </ul>
      <h3>Attaquer votre adversaire</h3>
      <p>Si vous avez des cartes sur le plateau, vous pouvez attaquer votre adversaire des façons suivantes:</p>
      <ul>
        <li>
          Si votre adversaire n'a pas posé de carte sur le plateau, vous pouvez directement attaquer votre adversaire;
          dans ce cas votre adversaire perd <strong>1 point de vie</strong> pour chaque point d'attaque de votre champion.
        </li>
        <li>
          Sinon, vous pouvez attaquer l'une des cartes de votre adversaire.
          Le résultat de l'attaque est alors déterminé par les règles suivantes:
          <ul>
            <li>Si l'attaque est meilleure que la défense, l'attaque <strong>réussit</strong>; dans ce cas:
              <ul>
                <li>le champion de la défense est retirée du jeu;</li>
                <li>
                  le défenseur perd <strong>1 point de vie</strong> pour chaque point de différence entre l'attaque et la défense.
                </li>
              </ul>
            </li>
            <li>Si l'attaque et la défense sont égales, la carte attaquante et la carte défendante sont détruites.</li>
            <li>
              Si la défense est supérieure à l'attaque, l'attaque est <strong>contrée</strong> et la carte attaquante est détruite.
            </li>
          </ul>
        </li>
      </ul>
    </section>
  );
}

export default Home;
