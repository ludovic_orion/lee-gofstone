import React from "react";

class Card extends React.Component {
  
    
    render() {
        const championImgUrl =
        "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/" +
        this.props.champion.key +
        "_0.jpg";
        return (
            <div
                onClick={this.props.clickHandler.bind(this, this.props.champion.id)}
            >
                    <div className="main card carte">
                        <img className="card-img-top" src={championImgUrl} alt={this.props.champion.name} />
                        <div className="mainCard card-body ">
                            <p className="mainText taille text-muted card-title">{this.props.champion.name}</p>
                            <p className="mainText taille text-muted card-title">Att : {this.props.champion.info.attack}</p>
                            <p className="mainText taille text-muted card-title">Def : {this.props.champion.info.defense}</p>
                        </div>
                    </div>
            </div>
        );
    }
}

export default Card;