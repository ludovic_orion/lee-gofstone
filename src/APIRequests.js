import axios from "axios"
import { baseUrl } from "./config";
function getHeaders(pToken){
    return {headers :{"www-authenticate" : pToken.toString()}}
}
function buildUrl(pPath){
    return baseUrl+pPath
}
function matchmakingUrl(pPath){
    return buildUrl("/matchmaking"+pPath)
}
function matchUrl(pPath) {
    return buildUrl("/match"+pPath)
}

export const createAccount = (pseudo,mail,mdp) => {
    axios.put(buildUrl("/user"), {
        "name": pseudo,
        "email": mail,
        "password": mdp
    })
    
}

export const connexion = (password,email,header) => {
    try {
        return axios.post(buildUrl("/login"),{
            "password": password,
            "email":email
            },
            header)
    } catch(e) {
        throw e.response.data;
    }
}

export const desinscrire = (password,email,pToken) => {
    return axios.get(buildUrl("/users/unsubscribe?password="+password+"&email="+email),getHeaders(pToken)
        
    )
}

export const getAllMatchmaking =  (pToken)=>
{
    return axios.get(matchmakingUrl('/getAll'),getHeaders(pToken))
}

export const getMatchmakingParticipate =  (pToken) => {
    return axios.get(matchmakingUrl('/participate'),getHeaders(pToken))
}

export const getMatchmakingUnParticipate =  (pToken) => {
    return axios.get(matchmakingUrl('/unparticipate'),getHeaders(pToken))
}

export const disconnect =  (pToken) => {
    return axios.post(buildUrl("/logout"),{},getHeaders(pToken))
}
export const acceptRequest = (pToken,mmId) => {
    return axios.get(matchmakingUrl("/acceptRequest?matchmakingId="+mmId),getHeaders(pToken))
}

export const askPlayerRequest = (pToken,mmId) => {
    return axios.get(matchmakingUrl("/request?matchmakingId="+mmId),getHeaders(pToken))
}
export const infoMatch = (pToken) => {
    return axios.get(matchUrl("/getMatch"),getHeaders(pToken))
    
}
export const attaquerCarte = (pToken,attack,defend) => {
    return axios.get(matchUrl("/attack?card="+attack+"&ennemyCard="+defend),getHeaders(pToken))
}
export const attaquerJoueur = (pToken,attack) => {
    return axios.get(matchUrl("/attackPlayer?card="+attack),getHeaders(pToken))
}
export const endTurn = (pToken) => {
    return axios.get(matchUrl("/endTurn"),getHeaders(pToken))
}
export const finirMatch = (pToken) => {
    return axios.get(matchUrl("/finishMatch"),getHeaders(pToken))
}


export const playCard = (pToken,key) => {
    return axios.get(matchUrl("/playCard?card="+key),getHeaders(pToken))
}

export const pickCard = (pToken) => {
    return axios.get(matchUrl('/pickCard'),getHeaders(pToken))
}
export const initDeck = (pToken,deckdeck) => {
    axios.get(matchUrl("/initDeck?deck="+deckdeck),getHeaders(pToken));
}

export const afficherCartes = () => {
    return axios.get(buildUrl("/cards"))
}