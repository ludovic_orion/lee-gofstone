import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/Main.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,

} from "react-router-dom";
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import FormulaireCompte from './FormulaireCompte';
import Connexion from './Connexion';
import Home from './Home';
import MenuCarte from './MenuCarte';
import Nav from './Nav';
import rootReducer from './reducers/reducer';
import Desinscription from './Desinscription';
import Deconnexion from './Deconnexion';
import Matchmaking from './Matchmaking';
import Match from './Match';
import ConsulterCartes from './ConsulterCartes';
const store = createStore(rootReducer, window.REDUX_DEVTOOLS_EXTENSION && window.REDUX_DEVTOOLS_EXTENSION());
store.subscribe(() => console.log(store.getState()))

//Router avec tous les composants appelés
function Main() {
  return (
    <Router>
      <Provider store={store}>
        <div className="App">
          <Nav />
          <Switch>
            <Route path="/FormulaireCompte">
              <FormulaireCompte />
            </Route>
            <Route path="/Matchmaking">
              <Matchmaking />
            </Route>
            <Route path="/Match">
              <Match />
            </Route>
            <Route path="/Connexion">
              <Connexion />
            </Route>
            <Route path="/Deconnexion">
              <Deconnexion />
            </Route>
            <Route path="/Desinscription">
              <Desinscription />
            </Route>
            <Route path="/ConsulterCartes">
              <ConsulterCartes/>
            </Route>
            <Route path="/MenuCarte">
              <MenuCarte nbCards={20} />
            </Route>
            <Route path="/">
              <Home />
            </Route>


          </Switch>
        </div>
      </Provider>
    </Router>
  );
}

export default Main;
