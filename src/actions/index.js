import { USERCONNECTED,USERDISCONNECTED} from "./types";

export function userConnected(emailUser,idUser,nameUser,tokeng) {
    
    return {
        type : USERCONNECTED,
        email : emailUser,
        id : idUser,
        name : nameUser,
        token : tokeng
    }
}

export function userDisconnected() {

    return {
        type : USERDISCONNECTED,
    }
}