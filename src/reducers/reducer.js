import { USERCONNECTED, USERDISCONNECTED } from "../actions/types";

let initialState = {isTokenOk : false, tokenValue : "",id:"",name:"",email:""}
function TokenState(pState = initialState, pAction) {
    switch (pAction.type) {
        case USERCONNECTED:
            return {
                isTokenOk : true, 
                tokenValue: pAction.token,
                id: pAction.id,
                name: pAction.name,
                email: pAction.email
            }
        case USERDISCONNECTED:
            return {
                isTokenOk : false, 
                tokenValue : "",
                id:"",
                name:"",
                email:""
            }
        default :
            return pState
    }



}

export default TokenState;