import { useState } from 'react';
import { Field, Form } from 'react-final-form';
import { Redirect, Link } from 'react-router-dom';
import './css/Connexion.css';
import { connexion } from './APIRequests'
import { useDispatch } from 'react-redux';
import { userConnected } from './actions';
import './css/form.css';
import Alert from 'react-bootstrap/Alert';


function Connexion() {
    const [etat, setEtat] = useState(false); //Initialisé à false passe à true si la connexion est reussie
    const header = {headers :{"Content-Type": "application/json"}};
    const [err, setErr] = useState(null);
    let dispatch = useDispatch();


    const onSubmit = async (data) => {
        //Connexion avec le mot de passe et le mail fournit dans le formulaire
        await connexion(data.password,data.mail,header)
        .then( (response) => {
                console.log(response)
                setEtat(true);
                dispatch(userConnected(response.data.email, response.data.id, response.data.name, response.data.token))
            }).catch(() => {
                setErr("identifiant ou mot de passe incorrect")
            })


        

    }

    document.title = "League of Stones - Connexion"

    

    //Redirection de la page lorsque l'on est connecté

    if (etat) {
        return <Redirect to="/Home" />
        
    } 
    //Formulaire de connexion demandant l'adresse mail et le mot de passe
    //On peut également créer un compte un compte en cliquant sur le bouton redirigeant vers le formulaire
    else {

        return (
            <div className="container ctn">
                <h1 className="text-white display-4">Connexion</h1>
                <Form
                    onSubmit={onSubmit}
                    render={({ handleSubmit }) => (
                        <form className="form" onSubmit={handleSubmit}>
                            <div className="form-group">
                                <Field
                                    name="mail"
                                    component="input"
                                    placeholder="Saisissez votre adresse mail"
				    type="email"
                                    className="form-control"
                                />
                            </div>
                            <div className="form-group">
                                <Field
                                    name="password"
                                    component="input"
                                    placeholder="Saisissez votre mot de passe"
                                    type="password"
                                    className="form-control"
                                />
                            </div >
                            <button className="btn btn-primary" type="submit">
                                Se connecter
                            </button>
                            <div className="scdLink">
                                <Link className="txt" to="/FormulaireCompte">Pas encore de compte ? Inscris toi !</Link>
                            </div>
                        </form>


                    )}
                />
                <Alert show={!!err} variant="danger" onClose={() => setErr("")} dismissible>{err}</Alert>
            </div>




        )
    }
}
export default Connexion;
