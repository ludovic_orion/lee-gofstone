import React from "react";
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Dropdown from 'react-bootstrap/Dropdown';
import './css/nav.css'

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <button
        className="titleDrop"
        ref={ref}
        onClick={(e) => {
            e.preventDefault();
            onClick(e);
        }}
    >
        {children}
    </button>
));

function Nav() {
    const isTokenOk = useSelector((state) => state.isTokenOk)
    const name = useSelector((state) => state.name);

    if (isTokenOk) {

        return (
            <header className="bg-dark">
                <div class="container-fluid">
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <div>
                            <Link className='navbar-brand' to="/Home" >League Of Stones</Link>
                        </div>
                    
                        <Dropdown className="txt bg-black">
                            <Dropdown.Toggle as={CustomToggle}>
                                {name}
                            </Dropdown.Toggle>

                            <Dropdown.Menu className="dropdwn txt">
                                <Dropdown.Item className="items">
                                    <Link className="txt" to="/Desinscription">Se désinscrire</Link>
                                </Dropdown.Item>

                                <Dropdown.Item className="items">
                                    <Link className="txt" to="/Deconnexion">Se déconnecter</Link>
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                        
                    </nav>
                </div>
            </header>
        );
    } else {
        return (
            <header className="bg-dark">
                <div class="container-fluid">
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <div>
                            <Link className='navbar-brand' to="/Home" >League Of Stones</Link>
                        </div>
                        <Dropdown className="bg-black">
                            <Dropdown.Toggle as={CustomToggle}>
                                Mon Compte
                            </Dropdown.Toggle>

                            <Dropdown.Menu className="dropdwn txt">
                                <Dropdown.Item className="items">
                                    <Link className="txt" to="/Connexion">Se connecter</Link>
                                </Dropdown.Item>

                                <Dropdown.Item className="items">
                                    <Link className="txt" to="/FormulaireCompte">S'inscrire</Link>
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                        
                    </nav>
                </div>

            </header>
        );
    }

}

export default Nav;
