import { useEffect,useState } from "react";
import { afficherCartes } from "./APIRequests.js";
import "./css/ConsulterCartes.css";
import { Link } from 'react-router-dom';
function ConsulterCartes() {
    const [allCards, setAllCards] = useState(null);
    useEffect(() => {
        const affichage = async () => {
            if (allCards == null) {
                let resultcards = await afficherCartes()
                setAllCards(resultcards)
            }
        }
        affichage();
    })
    let totalCartes = null;
    if (allCards && totalCartes === null) {
        totalCartes = allCards.data.map((carte) => {
            const championImgUrl = "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/" + carte.key + "_0.jpg";
            return (
                <article>
                    <div className="main card carte" >
                        <img className="card-img-top" src={championImgUrl} alt={carte.key} />
                        <div className="mainCard card-body ">
                            <p className="mainText taille text-muted card-title">{carte.name}</p>
                            <p className="mainText taille text-muted card-title">Att : {carte.info.attack}</p>
                            <p className="mainText taille text-muted card-title">Def : {carte.info.defense}</p>
                        </div>
                    </div>
                </article>
            )
        }
        )
    }

    if (allCards) {
        return (
            <main id="bouton_retour" className="container-fluid flex-fill d-flex flex-column justify-content-center p-0">
                
                <section className="container-fluid flex-fill py-2 d-flex flex-column justify-content-center text-light">
                    <h2 className="titre_cards">Champions disponibles dans le jeu</h2>
                    <div  class="play-btn row justify-content-center">
                        <Link className="btn btn-dark btn-lg" to="/Home">Retourner à l'accueil</Link>

                    </div>
                    <section className="row justify-content-center pb-2">
                        {totalCartes}
                    </section>
                    <article>
                    <div  class="play-btn row justify-content-center">
                        <a className="btn btn-dark btn-lg" href="#bouton_retour">Retour en haut de la page</a>
                        </div>
                    </article>


                </section>
                
                
            </main>
        )
    }
    else {
        return (<h1>Chargement en cours...</h1>)
    }

}
export default ConsulterCartes;
/**/
                    /*<div className="row justify-content-center">
                    <button class="btn btn-danger row justify-content-center">
                        <Link className="txtBtn" to="/Home">Accueil</Link>
                    </button>
                </div> */