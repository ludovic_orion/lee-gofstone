import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Signin from './Signin';
import Signup from './Signup';
import Home from './Signup';

//<li class="nav-item"><Link className="nav-link active" to="/">Accueil</Link></li>

function App() {
  return (
    <Router>
    <div className="App">
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <ul className="navbar-nav">
      <li className="nav-item"><Link className="nav-link active" to="/signin">Se connecter</Link></li>
      <li className="nav-item"><Link className="nav-link active" to="/signup">S'inscrire</Link></li>
      </ul>
      </nav>
      <Switch>
          <Route path="/signin">
            <Signin />
          </Route>
          <Route path="/signup">
            <Signup />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
    </div>
    </Router>
  );
}

export default App;
