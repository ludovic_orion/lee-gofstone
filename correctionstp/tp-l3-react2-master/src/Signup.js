import React from "react";
import { Form, Field } from "react-final-form";
import { baseUrl } from "./config";

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = { success: false };
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit(data) {
    if(data.password !== data.passwordConfirmation){
      console.error("password and confirmation are different")
      return;
    }
    const lData = {
      password: data.password,
      email: data.pseudo,
      name: data.pseudo,
    };
    const lHeaders = new Headers();
    lHeaders.append("Content-Type", "application/json");

    fetch(`${baseUrl}/user`, {
      method: "PUT",
      body: JSON.stringify(lData),
      headers:lHeaders
    }).then((e) => {
      if(e.status!==200){
        console.error(e);
        throw new Error("Bad request");
      }
      this.setState({ success: true });
    }).catch(e=>{
      this.setState({ success: false });
    });
  }
  render() {
    const form = (
      <div>
        <h1>Inscription</h1>
        <Form
          onSubmit={this.onSubmit}
          render={({ handleSubmit }) => (
            <form className="form" onSubmit={handleSubmit}>
              <div className="form-group">
                <label>Pseudo</label>
                <Field
                  name="pseudo"
                  component="input"
                  placeholder="Pseudo"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label>Mot de passe</label>
                <Field
                  name="password"
                  component="input"
                  placeholder="Mot de passe"
                  type="password"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label>Confirmer votre mot de passe</label>
                <Field
                  name="passwordConfirmation"
                  component="input"
                  placeholder="Mot de passe"
                  type="password"
                  className="form-control"
                />
              </div>
              <button className="btn btn-primary" type="submit">
                Valider
              </button>
            </form>
          )}
        />
      </div>
    );
    const success = (
      <div>
        <h2>Votre compte a bien été créé!</h2>
      </div>
    );
    const display = this.state.success ? success : form;
    return <div className="p-5">{display}</div>;
  }
}
export default Signup;
